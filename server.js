
// +------------------------------+
// | Simple TCP Chat 🙂 - SZERVER |
// +------------------------------+

var net = require('net');
var socket;

var server = net.createServer(
  function (client) {
    process.stdout.write('*** Kliens csatlakozott a(z) ' + client.remoteAddress + ' IP címről (távoli portszám: ' + client.remotePort + ')! ***\n');
    socket = client;
    client.on('data',
      function (buffer) {
        process.stdout.write('Re: ' + buffer);
      }
    );
  }
);

process.stdin.on('data',  
  function (buffer) {
    socket.write(buffer);
  }
)

server.listen(3000);
