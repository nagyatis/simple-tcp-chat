
// +-----------------------------+
// | Simple TCP Chat 🙂 - KLIENS |
// +-----------------------------+

var net = require('net');
var socket = net.Socket();

socket.connect(3000, 'localhost');

socket.on('connect', function () {
  process.stdout.write('*** Csatlakozva a(z) ' + socket.remoteAddress + ' IP című szerverhez (távoli portszám: ' + socket.remotePort + ')! ***\n');

  process.stdin.on('data',
    function (buffer) {
      socket.write(buffer);
    }
  )

  socket.on('data',
    function (buffer) {
      process.stdout.write('Re: ' + buffer);
    }
  );

});
